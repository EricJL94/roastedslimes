﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [NonSerialized]
    public bool ready = false;
    
    public void SetReady() {
        ready = !ready;
        GameController.instance.CheckForPlayersReady();
    }
}
