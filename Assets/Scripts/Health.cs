﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Health : MonoBehaviour {

    public int health;
    
    public int maxHealth;

    protected void Start () {
        health = maxHealth;
	}
	
    public virtual void TakeDamage(int damage) {
        health -= damage;
        //health = health <= 0 ? 0: health;
        if (health <= 0) {
            Die();
        } else if (health > maxHealth) {
            health = maxHealth;
        }
    }

    public abstract void Die();
}
