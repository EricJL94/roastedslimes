﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingPong : MonoBehaviour {

    public GameObject pos1;
    public GameObject pos2;
    public float speed;
    public GameObject[] todeactivate;
    public GameObject enemy;
    private float starttime;

    private void Start() {
        starttime = Time.time;
    }
    void Update() {
        transform.position = Vector2.Lerp(pos1.transform.position, pos2.transform.position, Mathf.PingPong((Time.time-starttime)*speed, 1f));

        if (Vector2.Distance(transform.position, pos2.transform.position)<1) {
            enemy.GetComponent<SpriteRenderer>().flipX = true;
            foreach (GameObject player in todeactivate) {
                player.SetActive(true);
            }
        }
        if (Vector2.Distance(transform.position, pos1.transform.position)<1) {
            enemy.GetComponent<SpriteRenderer>().flipX = false;
            foreach (GameObject player in todeactivate) {
                player.SetActive(false);
            }
        }
    }
}
