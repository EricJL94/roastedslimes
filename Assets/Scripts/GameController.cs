﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance = null;
    //public BoardManager boardManager;

    [NonSerialized]
    public bool started = false;

    [NonSerialized]
    public int score = 0;
    [NonSerialized]
    public int lives = 3;
    public GameObject[] hearts = new GameObject[3];

    [NonSerialized]
    public int level = 1;
    
    private int enemiesLeft;

    private Dictionary<string, GameObject> enemiesActives = new Dictionary<string, GameObject>();
    private int enemiesToSpawn;

    public Dictionary<string, GameObject> playersActives = new Dictionary<string, GameObject>();
    
    [SerializeField]
    private Text countdownText = default;
    [SerializeField]
    private Text scoreText = default;
    private CameraManager cameraManager;

    private GameObject enemyRespawn;

    void Start() {
        instance = this;
        cameraManager = Camera.main.GetComponent<CameraManager>(); ;
    }

    #region Level Management

    internal void FirstLevel() {
        OpenRoom();
        Invoke("InitLevel", 5);
    }

    internal void InitLevel() {
        cameraManager.endedLevel = false;

        enemiesToSpawn =
            (int) ((level / 3f) + 3) *
            playersActives.Count;

        enemiesLeft = enemiesToSpawn;
        OpenRoom();
        Invoke("ManageSpawning", 1);
    }

    private void ManageSpawning() {
        enemyRespawn = GameObject.FindGameObjectWithTag("EnemyRespawn");
        for (int i = 0; i < enemiesToSpawn; i++) {
            Invoke("SpawnEnemies", i);
        }
    }

    private void SpawnEnemies() {
        enemyRespawn.GetComponent<EnemySpawner>().SpawnEnemy();
    }

    private void OpenRoom() {
        LevelGenerator.instance.lastRoom.transform.Find("NextRoom").gameObject.SetActive(true);
    }

    private void CheckForEndedLevel() {
        if (enemiesLeft <= 0) {
            NextLevel();
        }
    }

    private void NextLevel() {
        cameraManager.endedLevel = true;
        level++;
        enemyRespawn.tag = "Untagged";
    }

    public static void callForInit() {
        if (instance.enemiesLeft <= 0) {
            instance.InitLevel();
        }
    }

    #endregion

    #region Player Ready Management

    public void CheckForPlayersReady() {
        StopCountdown();
        if (playersActives.Count == 0) {
            return;
        }

        foreach (string key in playersActives.Keys) {
            if (!playersActives[key].GetComponent<Player>().ready) {
                return;
            }
        }
        StartCountdown();
    }

    private void DeactivatePlayers() {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
            if (!playersActives.ContainsKey(player.name)) {
                player.gameObject.SetActive(false);
            }
        }
    }
    #endregion

    #region UI

    public void UpdateScoreText() {
        scoreText.text = "Score: " + score;
    }

    public void AddLives(int toAdd) {
        lives += toAdd;
        if (lives <= 0) {
            Stats.lastscore = score;
            SceneManager.LoadScene("Menu");
        }
        while (lives > 3) {
            lives--;
            score += 100;
            UpdateScoreText();
        }
        int i;
        for (i = 0; i < lives; i++) {
            hearts[i].SetActive(true);
        }
        for (; i < 3; i++) {
            GameObject heart = GameObject.Find("Heart" + i);
            hearts[i].SetActive(false);
        }
    }

    private void StartCountdown() {
        StartCoroutine(Countdown(5));
        countdownText.gameObject.SetActive(true);
    }

    private void StopCountdown() {
        StopAllCoroutines();
        countdownText.gameObject.SetActive(false);
    }

    private IEnumerator Countdown(float endTime = 5) {
        endTime += Time.time;
        while (endTime > Time.time) {
            countdownText.text = Mathf.Round(endTime - Time.time) + "";
            yield return new WaitForSeconds(0.5f);
        }
        DeactivatePlayers();
        started = true;
        GameObject.Find("Menu").SetActive(false);
        FirstLevel();
        StopCoroutine(Countdown());
    }
    #endregion

    #region Players & Enemies

    //Manage enemies dictionary (ctrl + m + h / ctrl + m + u)
    public static void RegisterEnemy(GameObject _enemy) {
        instance.enemiesActives.Add(_enemy.name, _enemy);
    }

    public static void UnregisterEnemy(GameObject _enemy) {
        instance.EnemyKilled(1);
        instance.enemiesActives.Remove(_enemy.name);
    }

    public static GameObject GetEnemy(string _netID) {
        return instance.enemiesActives[_netID];
    }

    //Manage player dictionary
    public static void RegisterPlayer(GameObject _player) {
        instance.playersActives.Add(_player.name, _player);
    }

    public static void UnregisterPlayer(string _netID) {
        instance.playersActives.Remove(_netID);
    }

    public static GameObject GetPlayer(string _netID) {
        return instance.playersActives[_netID];
    }

    public static GameObject GetRandomPlayer() {
        return instance.playersActives.Values.ToArray()[UnityEngine.Random.Range(0, instance.playersActives.Count)];
    }

    public void PlayerDead(GameObject player) {
        player.GetComponent<PlayerHealth>().RestoreHealth();
        player.transform.position = GetNewPositionFor();
        AddLives(-1);

    }

    private Vector2 GetNewPositionFor() {
        return Camera.main.transform.position;
    }


    private void EnemyKilled(int deaths) {
        enemiesLeft -= deaths;
        score += deaths;
        CheckForEndedLevel();
        UpdateScoreText();
    }

    #endregion
}
