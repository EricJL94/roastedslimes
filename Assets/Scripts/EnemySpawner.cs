﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    [Serializable]
    public struct SpawneableObject {
        public GameObject spawneableGameObject;
        public float chance;
    }

    public SpawneableObject[] objects;

    float totalchance = 0f;
    void Awake() {
        foreach (SpawneableObject o in objects) {
            totalchance += o.chance;
        }
    }

    public void SpawnEnemy() {
        float rand = UnityEngine.Random.Range(0, totalchance);
        int i = 0;
        while (objects[i].chance <= rand) {
            rand -= objects[i].chance;
            i++;
        }

        GameObject spawnedObject = Instantiate(objects[i].spawneableGameObject, transform.position, Quaternion.identity);

        spawnedObject.GetComponent<EnemyHealth>().maxHealth +=
            GameController.instance.level - 1;

        GameController.RegisterEnemy(spawnedObject);
    }
}
