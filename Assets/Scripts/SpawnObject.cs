﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {
    
    [Serializable]
    public struct SpawneableObject {
        public GameObject spawneableGameObject;
        public float chance;
    }

    public SpawneableObject[] objects;

    void Awake() {
        float totalchance = 0f;
        foreach (SpawneableObject o in objects) {
            totalchance += o.chance;
        }

        float random = UnityEngine.Random.Range(0, totalchance);
        int i = 0;
        while (objects[i].chance <= random) {
            random -= objects[i].chance;
            i++;
        }
            int rand = UnityEngine.Random.Range(0, objects.Length);
            Instantiate(objects[i].spawneableGameObject,
                transform.position,
                Quaternion.identity,
                this.transform);
    }
}