﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFire : MonoBehaviour {
    public GameObject[] objects;

    void Awake() {
        int rand = Random.Range(0, objects.Length);
        GameObject spawnedObject = Instantiate(objects[rand], transform.position, Quaternion.identity);
        spawnedObject.transform.parent = transform;
        spawnedObject.transform.Rotate(0, 0, -90);
    }
}