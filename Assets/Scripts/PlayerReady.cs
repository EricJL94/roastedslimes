﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerReady : MonoBehaviour {
    public Text playingText;
    public Text readyText;
    public Player player;

    private void Update() {
        readyText.gameObject.SetActive(player.ready);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player") && !GameController.instance.started) {
            GameController.RegisterPlayer(other.gameObject);
            playingText.text = other.name + "\nPlaying";
            GameController.instance.CheckForPlayersReady();
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.CompareTag("Player") && !GameController.instance.started) {
            foreach (string key in GameController.instance.playersActives.Keys) {
                GameController.instance.playersActives[key].GetComponent<Player>().ready = false;
            }
            GameController.UnregisterPlayer(other.name);
            playingText.text = other.name + "\nNot playing";
            readyText.gameObject.SetActive(false);
            GameController.instance.CheckForPlayersReady();
        }
    }
}
