﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitTrigger : MonoBehaviour {
    public int damage = 1;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<PlayerHealth>().TakeDamage(damage);
        }

        if (other.CompareTag("Projectile")) {
            this.transform.parent.GetComponent<EnemyHealth>().TakeDamage(other.GetComponent<Projectile>().damage);
            Destroy(other.gameObject);
        }
    }
}
