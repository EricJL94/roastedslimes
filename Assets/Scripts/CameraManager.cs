﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public bool endedLevel = false;

    private Rigidbody2D rb;
    public float speed;
    public float displacement;

    private void Start() {
        InvokeRepeating("RemoveRooms", 30, 10);
        rb = GetComponent<Rigidbody2D>();
    }
    void Update() {
        if (GameController.instance.started) {
            Vector3 newPosition = transform.position;
            newPosition.x = LevelGenerator.instance.lastRoom.transform.position.x + displacement;

            if (Vector2.Distance(transform.position, newPosition) < 0.2) {
                GameController.callForInit();
            }

            transform.position = Vector3.MoveTowards(
                transform.position,
                newPosition,
                Time.deltaTime * (endedLevel ? speed*2 : speed));
        }
    }

    private void RemoveRooms() {
        if (GameController.instance.started) {
            while (Vector2.Distance(LevelGenerator.PeekRoom().transform.position, transform.position) > 30) {
                Destroy(LevelGenerator.RemoveRoom());
            }
        }
    }
}