﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowStats : MonoBehaviour {

    private void OnGUI() {
        GUILayout.BeginArea(new Rect(0, 0, 200, 500));
        GUILayout.BeginVertical();

        foreach (string key in Stats.instance.statsDictionary.Keys) {
            GUILayout.Label(key + " - " + Stats.instance.statsDictionary[key].value);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

}
