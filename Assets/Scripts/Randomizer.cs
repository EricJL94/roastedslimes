﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : MonoBehaviour {

    Transform firstCorner;
    Transform secondCorner;
    public GameObject spawner;
    public int itemsToSpawn = 1;

    public PlacesTaken placesTaken;

    void Start() {
        firstCorner = transform.Find("First corner");
        secondCorner = transform.Find("Second corner");
        Vector2 spawnPosition;

        for (int i = 0; i < itemsToSpawn; i++) {
            do {
                spawnPosition = CalculateRandom();

            } while (placesTaken.IsTaken(spawnPosition));
            Instantiate(spawner, spawnPosition, Quaternion.identity, this.transform);
        }
    }

    public Vector2 CalculateRandom() {
        return new Vector2(
                    ((int) Random.Range(firstCorner.position.x, secondCorner.position.x)) + 0.5f,
                    ((int) Random.Range(firstCorner.position.y, secondCorner.position.y)) + 0.5f
                    );
    }
}
