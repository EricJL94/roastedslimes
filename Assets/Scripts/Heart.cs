﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour {

    public void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            GameController.instance.AddLives(1);
            Destroy(this.gameObject);
        }
    }
}

