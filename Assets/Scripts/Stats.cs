﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {
    
    public ShowStats label;

    public static Stats instance;

    [NonSerialized]
    public static int highscore;
    [NonSerialized]
    public static int lastscore = 0;

    public static Array highscores = new int[4];

    public const string FIRE_RATE = "Fire Rate";
    public const string MOVE_SPEED = "Move Speed";
    public const string PROJECTILE_SPEED = "Projectile Speed";
    public const string DAMAGE = "Damage";
    public const string EXTRA_AMMO = "Extra Ammo";

    private int index;

    public Dictionary<string, StatObject> statsDictionary = new Dictionary<string, StatObject>();
    private string[] keys = new string[] { FIRE_RATE, MOVE_SPEED, PROJECTILE_SPEED, DAMAGE, EXTRA_AMMO };

    private void Awake() {

        instance = this;

        statsDictionary.Add(FIRE_RATE,
            new StatObject(FIRE_RATE, 1f, 0.5f));

        statsDictionary.Add(MOVE_SPEED,
            new StatObject(MOVE_SPEED, 1f, 0.1f));

        statsDictionary.Add(PROJECTILE_SPEED,
            new StatObject(PROJECTILE_SPEED, 1f, 0.15f));

        statsDictionary.Add(DAMAGE,
            new StatObject(DAMAGE, 10f, 2.5f));

        statsDictionary.Add(EXTRA_AMMO,
            new StatObject(EXTRA_AMMO, 0f, 1f));
        
        index = UnityEngine.Random.Range(0, keys.Length);
    }

    /*private void randommethod() {
        string s = "";
        int a = UnityEngine.Random.Range(1, 10);
        s += a + " - ";
        highscores.SetValue(a, 0);
        Array.Sort(highscores);
        for (int i = 0; i < highscores.Length; i++) {
            s +=highscores.GetValue(i);
        }
        Debug.Log(s);
    }*/

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            label.enabled = !label.enabled;
        }
    }

    public void modifyStat() {
        string stat = keys[index % keys.Length];
        statsDictionary[stat].Increase();
        index++;
    }

    public float Get(string stat) {
        return statsDictionary[stat].value;
    }

    private void OnDestroy() {
        if (lastscore > highscore) {
            highscore = lastscore;
        }
    }
}
