﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [SerializeField]
    protected float speed = default;

    protected Rigidbody2D rb;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
        Vector2 moveInput = new Vector2(
            Input.GetAxisRaw("Horizontal " + this.name),
            Input.GetAxisRaw("Vertical " + this.name));
        rb.velocity = moveInput.normalized * speed * Stats.instance.Get(Stats.MOVE_SPEED);
	}
}
