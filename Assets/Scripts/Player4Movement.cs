﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player4Movement : PlayerMovement {

    public GameObject arrow;
    private Vector2 auxPosition;

    void Update() {

        if (Input.GetMouseButtonDown(1)) {
            Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            arrow.transform.position = target;
            arrow.SetActive(true);
            rb.velocity = (arrow.transform.position -
                transform.position).normalized * speed * Stats.instance.Get(Stats.MOVE_SPEED);
        }

        if (arrow.activeSelf) {
            if (Vector2.Distance(transform.position, arrow.transform.position) < 0.1 || rb.velocity.magnitude < 1f) {
                arrow.SetActive(false);
                rb.velocity = Vector2.zero;
            } else {
            rb.velocity = (arrow.transform.position -
                    transform.position).normalized * speed * Stats.instance.Get(Stats.MOVE_SPEED);
            }
        } else {
            rb.velocity = Vector2.zero;
        }
    }
}
