﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Templates : MonoBehaviour {
    
    public static Templates instance;
    public GameObject[] basicTemplates;
    public GameObject[] defaultTemplates;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }
}
