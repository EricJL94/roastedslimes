﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

    public static LevelGenerator instance = null;

    private Queue<GameObject> roomsQueue = new Queue<GameObject>();
    public GameObject lastRoom;
    //public Queue<ParticleSystem> animations;
    private void Awake() {
        instance = this;
    }

    private void Start() {
        lastRoom = GameObject.Find("GameManager");
        //animations = new Queue<ParticleSystem>();
    }

    //Queue management
    public static void AddRoom(GameObject room) {
        instance.roomsQueue.Enqueue(room);
        Destroy(instance.lastRoom.transform.Find("Grid").transform.Find("WallToDestroy").gameObject);
        instance.lastRoom = room;
        //ClearAnimations();
    }

    /*private static void ClearAnimations() {
        while (instance.animations.Count > 0 ) {
            if (instance.animations.Peek().isPlaying) return;
                Destroy(instance.animations.Dequeue().gameObject);
        }
    }*/

    public static GameObject RemoveRoom() {
        return instance.roomsQueue.Dequeue();
    }

    public static GameObject PeekRoom() {
        return instance.roomsQueue.Peek();
    }
    public static void DeleteRoom() {
        Destroy(RemoveRoom());
    }

    /*public static void addAnimation(ParticleSystem particleSystem) {
        instance.animations.Enqueue(particleSystem);
    }*/
}