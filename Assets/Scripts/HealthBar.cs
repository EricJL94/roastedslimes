﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    public Image HPBar;

    public void ChangeHPBar(float normalizedHP) {
        HPBar.transform.localScale = new Vector2(normalizedHP, 1f);
    }

    public void SetImmuneColor(bool immune) {
        HPBar.GetComponent<Image>().color = immune ? Color.yellow : Color.red;
    }
}
