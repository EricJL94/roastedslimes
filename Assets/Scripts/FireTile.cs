﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTile : MonoBehaviour {

    private void OnTriggerStay2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            if (Vector2.Distance(transform.position, other.transform.position) < 1) {
                other.GetComponent<PlayerHealth>().TakeDamage(1);

            }
            return;
        }

        if (other.CompareTag("Crate")) {
            other.GetComponent<BoxCollider2D>().enabled = false;
            ParticleSystem particles = other.GetComponentInChildren<ParticleSystem>();
            particles.transform.parent = null;
            particles.Play();
            Destroy(other.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        try {
            if (other.CompareTag("Crate")) {
                ParticleSystem particles = other.GetComponentInChildren<ParticleSystem>();
                particles.transform.parent = null;
                particles.Play();
                particles.GetComponent<AudioSource>().Play();
                //LevelGenerator.addAnimation(particles);
                Destroy(other.gameObject);
            }
        } catch (System.NullReferenceException) {
        }
    }
}
