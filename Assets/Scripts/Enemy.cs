﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speed;
    public EnemyHitTrigger hitter;
    private GameObject objective = null;
    private Vector2 wanderObjective;
    private bool wandering = true;
    public EnemyShooter shooterScript = null;

    private void Awake() {
        name = "Enemy" + GetInstanceID();
    }

    void Start() {
        wanderObjective = transform.position;
    }

    void Update() {
        if (wandering) {
            if (objective != null) {
                wandering = false;
                return;
            }

            if (Vector2.Distance(transform.position, wanderObjective) < 0.5) {
                wanderObjective = transform.position + UnityEngine.Random.onUnitSphere.normalized * 2;
                try {
                    Vector2 playerPosition = GameController.GetRandomPlayer().transform.position;
                    wanderObjective = Vector2.Lerp(wanderObjective, playerPosition, 0.1f);
                } catch {

                }
            }

            transform.position = Vector2.MoveTowards(transform.position, wanderObjective, speed / 2 * Time.deltaTime);

        } else {
            transform.position = Vector2.MoveTowards(
                transform.position,
                objective.transform.position,
                speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {

        if (other.gameObject.CompareTag("Player")) {
            objective = other.gameObject;
            
            if (shooterScript != null) {
                shooterScript.objective = objective;
                shooterScript.enabled = true;
            }
        }
    }
}
