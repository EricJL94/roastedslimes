﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRoom : MonoBehaviour {
    private GameObject[] objects;
    public bool isDefault = false;

    void Awake() {
        objects = isDefault ? Templates.instance.defaultTemplates : Templates.instance.basicTemplates;

        int rand = Random.Range(0, objects.Length);
        GameObject spawnedObject = Instantiate(objects[rand], transform.position, Quaternion.identity);
        LevelGenerator.AddRoom(spawnedObject);
    }
}