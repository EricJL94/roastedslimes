﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button : MonoBehaviour {

    public Text scores;
    public void LoadScene() {
        SceneManager.LoadScene("SampleScene");
    }

    private void Start() {
        scores.text = "Last Score: " + Stats.lastscore +"\nHighscore: " + Stats.highscore;
    }
}
