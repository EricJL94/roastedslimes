﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    [SerializeField]
    private int baseAmmo = default;
    private int totalAmmo;
    public int ammo;

    private bool reloading = false;
    private float nextReload;
    public float fireRate;

    private float nextShoot = 0;
    public GameObject projectilePrefab;

    void Start() {
        ammo = baseAmmo + (int) Stats.instance.Get(Stats.EXTRA_AMMO);
    }

    void Update() {
        totalAmmo = baseAmmo + (int) Stats.instance.Get(Stats.EXTRA_AMMO);
        if (Time.time >= nextShoot && ammo > 0) {
            if (Input.GetAxisRaw("Shoot " + name) != 0) {
                nextShoot = Time.time + fireRate / Stats.instance.Get(Stats.FIRE_RATE);
                if (!GameController.instance.started) {
                    if (GameController.instance.playersActives.ContainsKey(name)) {
                        gameObject.GetComponent<Player>().SetReady();
                    }
                } else {
                    PreShooting();
                }
            }
        }
        if (reloading && Time.time >= nextReload) {
            if (ammo < totalAmmo) {
                ammo++;
                nextReload = Time.time + 1.5f * fireRate;
                ScaleTransform();
            } else {
                ammo = totalAmmo;
                reloading = false;
            }
        }
    }

    private void PreShooting() {
        if (!reloading) {
            nextReload = Time.time + 2 * fireRate;
        }
        reloading = true;
        ammo -= 1;
        ScaleTransform();
        ShootProjectile();
    }

    private void ScaleTransform() {
        float scale = Mathf.Lerp(0.75f, 1, (float) ammo / (float) totalAmmo);
        transform.localScale = new Vector2(scale, scale);
    }

    private void ShootProjectile() {
        GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity, transform);

        projectile.GetComponent<Rigidbody2D>().AddForce(Vector2.right.normalized * 500 * Stats.instance.Get(Stats.PROJECTILE_SPEED));


    }
}
