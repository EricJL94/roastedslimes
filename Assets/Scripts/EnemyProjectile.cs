﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour {

    private int damage;

    private void Start() {

        damage = transform.parent.GetComponentInChildren<EnemyHitTrigger>().damage;
        GetComponent<SpriteRenderer>().color = transform.parent.gameObject.GetComponent<SpriteRenderer>().color;
        transform.parent = null;
        Invoke("Despawn", 0.75f);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<PlayerHealth>().TakeDamage(damage);
        }
    }


    private void Despawn() {
        Destroy(gameObject);
    }
}
