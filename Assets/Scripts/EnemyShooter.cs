﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour {

    public float fireRate;
    public GameObject projectilePrefab;
    private float nextShoot = 0;
    public GameObject objective;
    private void Update() {
        if (Time.time >= nextShoot) {
            nextShoot = Time.time + fireRate;

            GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity, transform);

            projectile.GetComponent<Rigidbody2D>().AddForce((objective.transform.position - transform.position).normalized * 250);


        }
    }
}
