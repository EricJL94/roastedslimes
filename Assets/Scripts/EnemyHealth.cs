﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health {
    public ParticleSystem smoke;

    public override void Die() {
        smoke.transform.parent = null;
        smoke.Play();
        smoke.GetComponent<AudioSource>().Play();
        //LevelGenerator.addAnimation(smoke);
        GameController.UnregisterEnemy(gameObject);
        Destroy(gameObject);
    }
}
