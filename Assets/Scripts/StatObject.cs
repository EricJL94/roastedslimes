﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatObject {
    public string statName;
    public float value;
    public float increment;
    public float max;
    public bool maxed = false;

    public StatObject(string statName, float value, float increment, float max) {
        this.statName = statName;
        this.value = value;
        this.increment = increment;
        this.max = max;
    }

    public StatObject(string statName, float value, float increment) {
        this.statName = statName;
        this.value = value;
        this.increment = increment;
        max = value + 10 * increment;
    }

    public void Increase() {
        if (maxed) return;
        value += increment;
        maxed = value >= max ? true : false;
    }

}
