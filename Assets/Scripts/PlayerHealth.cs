﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : Health {

    private HealthBar HPBar;

    public bool immune = false;

#pragma warning disable CS0108 // El miembro oculta el miembro heredado. Falta una contraseña nueva
    private void Start() {
#pragma warning restore CS0108 // El miembro oculta el miembro heredado. Falta una contraseña nueva
        base.Start();
        HPBar = transform.Find("HealthBar").gameObject.GetComponent<HealthBar>();
    }
    public override void Die() {
        StopAllCoroutines();
        GameController.instance.PlayerDead(gameObject);
    }

    public override void TakeDamage(int damage) {
        if (!immune) {
            BeImmune();
            base.TakeDamage(damage);
            HPBar.ChangeHPBar((float) health / (float) maxHealth);
        }
    }

    private void NoImmune() {
        immune = false;
        HPBar.SetImmuneColor(immune);
    }

    private void BeImmune(float time = 1f) {
        immune = true;
        HPBar.SetImmuneColor(immune);
        Invoke("NoImmune", time);
    }

    public void RestoreHealth() {
        BeImmune(3);
        health = maxHealth;
        HPBar.ChangeHPBar((float) health / (float) maxHealth);
    }
}