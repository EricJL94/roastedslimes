﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [NonSerialized]
    public int damage;

    protected void Start() {
        GetComponent<SpriteRenderer>().color = transform.parent.gameObject.GetComponent<SpriteRenderer>().color;
        transform.parent = null;
        damage = (int)Stats.instance.Get(Stats.DAMAGE);
        Invoke("Despawn", 0.5f);
    }
    private void Despawn() {
        Destroy(gameObject);
    }
}
