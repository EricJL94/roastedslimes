﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {
    public Sprite spikesOn;
    public Sprite spikesOff;
    public BoxCollider2D boxCollider;
    public SpriteRenderer spriteRenderer;
    
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<PlayerHealth>().TakeDamage(1);
            boxCollider.enabled = false;
            spriteRenderer.sprite = spikesOff;
            Invoke("TriggerSpikes", 2);
        }
    }

    private void TriggerSpikes() {
        spriteRenderer.sprite = spikesOn;
        boxCollider.enabled = true;
    }
}
