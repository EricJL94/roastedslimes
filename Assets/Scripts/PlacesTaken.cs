﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacesTaken : MonoBehaviour {

    private ArrayList placesTaken = new ArrayList();

    internal void AddTaken(Vector2 spawnPosition) {
        placesTaken.Add(spawnPosition);
    }

    internal bool IsTaken(Vector2 spawnPosition) {
        if (placesTaken.Contains(spawnPosition)) {
            return true;
        } else {
            AddTaken(spawnPosition);
            return false;
        }
    }
}
