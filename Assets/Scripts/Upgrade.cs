﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour {

    internal Stats stats;

    private void Start() {
        stats = Stats.instance;
    }
    public void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            Stats.instance.modifyStat();
            Destroy(this.gameObject);
        }
    }
}
